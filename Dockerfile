# Use the official Airflow image as a base
FROM apache/airflow:2.7.1

# Set the AIRFLOW_HOME environment variable
ENV AIRFLOW_HOME=/opt/airflow

# Set the working directory
WORKDIR ${AIRFLOW_HOME}

USER root
RUN apt update && apt -y install procps default-jre
USER airflow

# Copy the requirements-docker.txt file from your project directory and install the dependencies
COPY requirements-docker.txt ./
RUN pip install --no-cache-dir -r requirements-docker.txt

# Copy your dags to the DAG folder in the container
COPY dags/ ./dags/
COPY spark/ ./spark/
COPY tests/ ./tests/

ENV PYTHONPATH="${PYTHONPATH}:${AIRFLOW_HOME}"