from datetime import datetime, timedelta
from airflow import DAG
from airflow.providers.apache.spark.operators.spark_submit import SparkSubmitOperator

default_args = {
    'owner': 'airflow',
    'retries': 1,
    'retry_delay': timedelta(minutes=1)
}

dag = DAG(
    'spark_temperature_analysis',
    default_args=default_args,
    description='A DAG to run Spark job for temperature analysis',
    schedule=timedelta(days=1),
    start_date=datetime(2023, 10, 20),
    catchup=False
)

spark_job = SparkSubmitOperator(
    task_id='run_temperature_analysis',
    application='/opt/airflow/spark/daily_temperature_spark_job.py',
    conn_id='spark_local',
    dag=dag
)

spark_job