from datetime import datetime
from airflow.decorators import task, dag
import random

default_args = {
    'owner': 'airflow',
    'start_date': datetime(2023, 9, 24),
}

@dag(schedule='@daily', default_args=default_args, catchup=False)
def temperature_analysis_dag():

    # Mock function to simulate extracting temperature data
    @task()
    def extract():
        # This mock data represents hourly temperature readings for a day
        return {hour: random.randint(15, 25) for hour in range(24)}

    @task()
    def transform(hourly_data: dict):
        daily_avg = sum(hourly_data.values()) / len(hourly_data)
        return daily_avg

    @task()
    def load(daily_avg: float):
        # For simplicity, we'll just print the average, but in a real scenario,
        # you might store this in a database or send it to another service
        print(f"Daily Average Temperature: {daily_avg:.2f}°C")

    hourly_temps = extract()
    daily_avg_temp = transform(hourly_temps)
    load(daily_avg_temp)

dag_instance = temperature_analysis_dag()
