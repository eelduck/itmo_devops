from pyspark import SparkConf
from pyspark.sql import SparkSession
import random

def init_spark_session():
    config = SparkConf().setAppName("My PySpark App").setMaster("spark://spark-master:7077")
    
    return SparkSession.builder.config(conf=config).getOrCreate()

def extract(spark: SparkSession):
    # This mock data represents hourly temperature readings for a day
    hourly_data = [(hour, random.randint(15, 25)) for hour in range(24)]
    
    # Convert the hourly_data list to a DataFrame
    df = spark.createDataFrame(hourly_data, ["hour", "temperature"])
    
    return df

def transform(df):
    # Calculate the average temperature
    avg_temp = df.agg({"temperature": "avg"}).collect()[0][0]
    
    return avg_temp

def load(avg_temp):
    # For simplicity, we'll just print the average, but in a real scenario,
    # you might store this in a database or send it to another service
    print(f"Daily Average Temperature: {avg_temp:.2f}°C")

def main():
    spark = init_spark_session()
    hourly_temps_df = extract(spark)
    daily_avg_temp = transform(hourly_temps_df)
    load(daily_avg_temp)
    spark.stop()

if __name__ == "__main__":
    main()