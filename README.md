# ITMO_DevOps

This repository provides a setup to run Apache Airflow using Docker Compose. It includes a sample DAG [daily_temprature_dag](/dags/daily_temprature_dag.py).

## DAG Description

### daily_temprature_dag

- Start Date: September 25, 2023
- Schedule: Runs daily at midnight.
- Tasks:
  - extract: Mocks hourly temprature data in Celsius
  - transform: Calculates daily average temprature
  - load: Prints daily temprature to the logs.

## Usage

1. Start Services:

   To start Airflow services, navigate to the root directory and run:

    ```bash
    docker-compose up -d
    ```   
   

3. Access Airflow Web UI:

   After starting the services, you can access the Airflow Web UI at http://localhost:8080/.
   - User: `airflow`
   - Password: `airflow`

   For the daily_temprature_dag:
   - Navigate to the DAG's tree view or graph view.
   - Click on the task instance box you're interested in to view logs or task details.

   For the spark_daily_temprature_dag:
   - First you need to create Spark connection:
      - Navigate to Admin -> Connections -> New
      - Connection Id: `spark_local`
      - Connection Type: `Spark`
      - Host: `spark://spark-master`
      - Port: `7077`
   - Then you can treat it like a normal DAG

4. View Airflow Logs:

   - Web UI: Access task logs via the Airflow Web UI as described above.
   - Docker Logs: To view service logs (webserver or scheduler), run:
    
        ```bash
        docker-compose logs webserver
        docker-compose logs scheduler
        ```

5. Stop Services:

   To stop Airflow and related services:
  
   ```bash
   docker-compose down
   ``` 

## Notes

This setup is intended for development purposes. If you plan to deploy Airflow in a production environment, additional configurations and security considerations are necessary.
