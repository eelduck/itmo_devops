# Change Log
All notable changes to this project will be documented in this file.

## 2023-09-25 - Lab 1
### Added
- Dag example 
- Dockerfile
- docker-compose.yml

## 2023-10-22 - Lab 2
### Added
- Spark [dag](/dags/spark_daily_temperature_dag.py)
- Spark [job](/spark/daily_temperature_spark_job.py) 

### Updated
- Dockerfile, docker-compose.yaml - Add Spark support
- README.md - Add instruction on how to add Spark connection

## 2023-10-24 - Lab 3
### Added
- Gitlab CI Jobs [config](.gitlab-ci.yml)
- Connected Gitlab Runner
- [Tests](/tests/) with pytest

### Updated
- Dockerfile - minor updates

## 2023-10-30 - Lab 4
### Added
- Logging and monitoring. Grafana + Zabbix + Loki + Promtail