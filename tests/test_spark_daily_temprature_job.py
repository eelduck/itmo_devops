import pytest
from pyspark.sql import SparkSession
from spark.daily_temperature_spark_job import extract, transform

@pytest.fixture(scope="module")
def spark_session():
    spark = SparkSession.builder \
        .master("local") \
        .appName("PySpark Test") \
        .getOrCreate()
    yield spark
    spark.stop()

def test_extract(spark_session):
    df = extract(spark_session)
    assert df.count() == 24
    assert df.columns == ["hour", "temperature"]

def test_transform(spark_session):
    hourly_data = [(hour, 20) for hour in range(24)]
    df = spark_session.createDataFrame(hourly_data, ["hour", "temperature"])
    avg_temp = transform(df)
    assert avg_temp == 20.0